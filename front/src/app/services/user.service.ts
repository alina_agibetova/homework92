import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginUserData, RegisterUserData, User, Users } from '../models/user.model';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  registerUser(userData: RegisterUserData){
    return this.http.post<User>('http://localhost:8000/users', userData);
  }

  getUsers() {
    return this.http.get<Users[]>('http://localhost:8000/users/all' ).pipe(
      map(response => {
        return response.map( users => {
          return new Users(
            users.username
          );
        });
      })
    );
  }

  login(userData: LoginUserData){
    return this.http.post<User>('http://localhost:8000/users/sessions', userData)
  }

  logout(){
    return this.http.delete('http://localhost:8000/users/sessions');
  }
}
