import { LoginError, RegisterError, User, Users } from '../models/user.model';


export type UsersState = {
  user: null | User,
  users: Users[],
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError
}


export type AppState = {
  users: UsersState,
}
