import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  fetchUsersFailure,
  fetchUsersRequest, fetchUsersSuccess,
  LoginUserFailure,
  LoginUserRequest,
  LoginUserSuccess,
  logoutUser,
  logoutUserRequest,
  registerUserFailure,
  registerUserRequest,
  registerUserSuccess
} from './users.actions';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { HelpersService } from '../services/helpers.service';

@Injectable()
export class UsersEffects{
  constructor(
    private actions: Actions,
    private userService: UserService,
    private router: Router,
    private snackbar: MatSnackBar,
    private helpers: HelpersService,
  ) {}

  registerUser = createEffect(() => this.actions.pipe(
    ofType(registerUserRequest),
    mergeMap(({userData}) => this.userService.registerUser(userData).pipe(
      map(user => registerUserSuccess({user})),
      tap(() => {
        this.helpers.openSnackbar('Register successful')
        void this.router.navigate(['/'])
      }),
      this.helpers.catchServerError(registerUserFailure)
    ))
  ));

  fetchUsers = createEffect(() => this.actions.pipe(
    ofType(fetchUsersRequest),
    mergeMap(() => this.userService.getUsers().pipe(
      map(users => fetchUsersSuccess({users})),
      catchError(() => {
        return of(fetchUsersFailure({error: 'Error'}));
      })
    ))
  ))

  loginUser = createEffect(() => this.actions.pipe(
    ofType(LoginUserRequest),
    mergeMap(({userData}) => this.userService.login(userData).pipe(
      map(user => LoginUserSuccess({user})),
      tap(() => {
        this.helpers.openSnackbar('Login successful')
        void this.router.navigate(['/'])
      }),
      this.helpers.catchServerError(LoginUserFailure)
    ))
  ));

  logoutUser = createEffect(() => this.actions.pipe(
    ofType(logoutUserRequest),
    mergeMap(() => {
      return this.userService.logout().pipe(
        map(() => logoutUser()),
        tap(() => {
          void this.router.navigate(['/']);
          this.helpers.openSnackbar('Logout successful')
        })
      );
    }))
  )
}
