import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { User, Users } from '../../models/user.model';
import { fetchUsersRequest } from '../../store/users.actions';

interface Message {
  user: string,
  message: string
}

interface ServerMessage {
  type: string,
  message: Message
}
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.sass']
})

export class MessagesComponent implements OnInit, OnDestroy {
  name!: string;
  token!: string;
  messageText = '';
  messages: Message[] = [];
  ws!: WebSocket;
  user: Observable<null | User>;
  users: Observable<Users[]>

  constructor(private store: Store<AppState>) {
    this.user = store.select(state => state.users.user);
    this.users = store.select(state => state.users.users)
  }

  ngOnInit(): void {
    this.ws = new WebSocket('ws://localhost:8000/chat');
    this.ws.onclose = () => console.log('ws closed');

    this.ws.onmessage = event => {
      const decodedMessage: ServerMessage = JSON.parse(event.data);

      if (decodedMessage.type === 'NEW_MESSAGE'){
        this.messages.length = 0;
        this.messages = this.messages.concat(decodedMessage.message);
      }
      this.store.dispatch(fetchUsersRequest())
    };
    this.user.subscribe(user => {
      this.name = <string>user?.username;
      this.token = <string>user?.token;
    })

    this.ws.onopen = () => {
      this.setUsername();
    }
  }


  setUsername() {
    this.ws.send(JSON.stringify({
      type: 'SET_USER',
      username: this.name,
      token: this.token
    }));
  }

  sendMessage() {
    this.ws.send(JSON.stringify({
      type: 'SEND_MESSAGE',
      message: this.messageText,
    }));

    this.messageText = '';
  }

  ngOnDestroy(): void {
  }

}
