export class Users {
  constructor(
    public username: string
  ) {
  }
}

export interface User {
  _id: string,
  email: string,
  token: string,
  username: string
}

export interface Users {
  username: string
}

export interface RegisterUserData {
  email: string;
  password: string;
  username: string;
}

export interface FieldError {
  message: string
}

export interface RegisterError {
  errors: {
    email: FieldError,
    password: FieldError,
    username: FieldError,
  }
}

export interface LoginUserData {
  email: string,
  password: string
}

export interface LoginError {
  error: string
}
