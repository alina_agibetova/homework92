const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');
const {nanoid} = require('nanoid');
const users = require('./app/users');
const Message = require("./models/Message");
const app = express();
require('express-ws')(app);


const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use('/users', users);

const activeConnections = {};

app.ws('/chat', (ws, req) => {
  const id = nanoid()
  console.log('client connected id=', id);
  activeConnections[id] = ws;

  let user = {
    username: String,
    token: String,
    datetime: String
  };

  ws.on('message', async msg => {
    const decodedMessage = JSON.parse(msg);
    if (decodedMessage.type === 'SEND_MESSAGE') {
      let msgData = {
        user: user.username,
        message: decodedMessage.message
      }

      const messages = new Message(msgData);
      await messages.save();
    }
    switch (decodedMessage.type){
      case 'SET_USER':
        user.username = decodedMessage.username;
        user.token = decodedMessage.token;
        let dataUser = {
          user: user.username,
          message: ''
        }

        const messageDataUser = new Message(dataUser);
        await messageDataUser.save();
        const allMessages = await Message.find().limit(30);
        Object.keys(activeConnections).forEach(id => {
          const conn = activeConnections[id];
          conn.send(JSON.stringify({
            type: 'NEW_MESSAGE',
            message: allMessages
          }));
        })
        break;
      case 'SEND_MESSAGE':
        if (user.token === ''){
          break
        }
        const array = await Message.find();
        Object.keys(activeConnections).forEach(id => {
          const conn = activeConnections[id];
          conn.send(JSON.stringify({
            type: 'NEW_MESSAGE',
            message: array
          }));
        });
        break;
      default:
        console.log('Unknown message type:', decodedMessage.type);
    }
  })

  ws.on('close', async (msg) => {
    let data = {
      user: user.username,
      message: '',
    }

    const message = new Message(data);
    await message.save();
    const messages = await Message.find();

    Object.keys(activeConnections).forEach(id => {
      const conn = activeConnections[id];
      conn.send(JSON.stringify({
        type: 'NEW_MESSAGE',
        message: messages
      }));
    });
    delete activeConnections[id];
  });
});

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });

  process.on('exit', () => {
    mongoose.disconnect();
  })
};

run().catch(e => console.error(e));