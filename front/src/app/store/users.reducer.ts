import { UsersState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  fetchUsersFailure,
  fetchUsersRequest, fetchUsersSuccess,
  LoginUserFailure,
  LoginUserRequest,
  LoginUserSuccess,
  logoutUser,
  registerUserFailure,
  registerUserRequest,
  registerUserSuccess
} from './users.actions';


const initialState: UsersState = {
  user: null,
  users: [],
  registerLoading: false,
  registerError: null,
  loginLoading: false,
  loginError: null
};

export const usersReducer = createReducer(
  initialState,
  on(registerUserRequest, state =>
    ({...state, registerLoading: true, registerError: null})),
  on(registerUserSuccess, (state, {user}) =>
    ({...state, registerLoading: false, user})),
  on(registerUserFailure, (state, {error}) =>
    ({...state, registerLoading: true, registerError: error})),

  on(fetchUsersRequest, state => ({...state, fetchLoading: true, fetchError: null})),
  on(fetchUsersSuccess, (state, {users}) => ({...state, fetchLoading: false, users})),
  on(fetchUsersFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(LoginUserRequest, state => ({...state, loginLoading: true, loginError: null})),
  on(LoginUserSuccess, (state, {user}) => ({...state, loginLoading: true, user})),
  on(LoginUserFailure, (state, {error}) => ({...state, loginLoading: true, loginError: error})),
  on(logoutUser, state => ({...state, user: null}))
);
